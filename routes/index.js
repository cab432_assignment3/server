const express = require('express');
const router = express.Router();
const ctrl = require('../controllers/db_controller');
// const queue = require('../controllers/tweet_queue');

/* GET method route  */
router.get('/tweet/:term', ctrl.scan);
// router.get('/tweet', ctrl.scan);

/* POST method route */
router.post('/tweet', ctrl.upload);
// router.post('/tweet', queue.sendTweetsToQueue);

/* POST test route */
router.get('/test', function(req,res,next){
    const MAX = 1000000000;
    let start = Date.now();
    for (let i=0; i<=MAX; i++) {
        let dummy = Math.log(i+1);
    }
    let timing = Date.now() - start;
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World\nThis is for testing\n' + timing + "(ms)\n");
});

module.exports = router;
