const AWS = require('aws-sdk');
const config = require('./config');
const analyzer = require('../api/SentimentAnalyzer');

AWS.config.update(config);

const docClient = new AWS.DynamoDB.DocumentClient();
const TABLE_NAME = 'tweet';

exports.upload = function (req, res)  {
    const tweet = JSON.parse(req.body.tweet);
    let topics = tweet.name.trim().split(',');
    const max = 450;
    let tweetText = (tweet.truncated ? tweet.extended_tweet.full_text : tweet.text).replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
    let isUpload = false;
    
    for (let i = 0; i < max; i++) {
        if (topics.some( function(topic) {
            return tweetText.includes(topic);
        })) {
            isUpload = true;
        }
        analyzer.sentimentAnalysis(tweet.text)
    }

    if (isUpload){
        let input = {
            "name": tweet.name,
            "text": tweet.text,
            "sentiment": analyzer.sentimentAnalysis(tweet.text)
        };
        
        let params = {
            TableName: TABLE_NAME,
            Item:  input
        };
    
        docClient.put(params, function (err, data) {
            if (err) {
                console.log("tweets::save::error - " + JSON.stringify(err, null, 2));                      
            } else {
                res.end();                   
            }
        });
    }
    res.end(); 
}

exports.scan = function (req, res)  {
    let term = req.params.term;
    
    var params = {
        TableName: TABLE_NAME,
        FilterExpression: "#name = :name",
        ExpressionAttributeNames: {
            "#name": "name"
        },
        ExpressionAttributeValues:{ ":name" : term }
    };
    // scan table
    docClient.scan(params, onScan);

    let setimentScore = {
        positive: 0,
        negative: 0,
        neutral: 0
    }

    let items = []
    let content = null;
    
    function onScan(err, data) {
        if (err) {
            console.log("users::scan::error - " + JSON.stringify(err, null, 2));
        }
        else {
            data.Items.forEach(function(tweet) {
                if (tweet.sentiment > 0) {
                    setimentScore.positive+=1;
                } else if (tweet.sentiment < 0) {
                    setimentScore.negative+=1;
                } else {
                    setimentScore.neutral+=1;
                }
                // for (let key in topices_scores) {
                //     if (tweet.text.toLowerCase().includes(key)) {
                //         topices_scores[key].scores += tweet.sentiment;
                //         topices_scores[key].num += 1;
                //     }
                // }
                content += ', ' + tweet.text;
            });
            // items.push(...data.Items)
            
            // continue scanning if we have more tweets
            if (typeof data.LastEvaluatedKey != "undefined") {
                console.log("Scanning for more...");
                params.ExclusiveStartKey = data.LastEvaluatedKey;
                docClient.scan(params, onScan);
            } else {
                const results = analyzer.wordFreq(analyzer.tokenize(content))
                // res.send({topices_scores, top_words: results, tweets: items});
                // res.send({topices_scores, top_words: results});
                res.send({topices_scores: setimentScore, top_words: results});
            }
        }
    }
}
