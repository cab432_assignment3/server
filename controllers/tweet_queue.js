// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'ap-southeast-2'});

// Create an SQS service object
var sqs = new AWS.SQS({apiVersion: '2012-11-05'});

var queueURL = "https://sqs.ap-southeast-2.amazonaws.com/449458006130/tweet-queue";

exports.getTweetsFromQueue = function (req, res)  {
    var params = {
    AttributeNames: [
        "SentTimestamp"
    ],
    MaxNumberOfMessages: 1,
    MessageAttributeNames: [
        "All"
    ],
    QueueUrl: queueURL,
    VisibilityTimeout: 20,
    WaitTimeSeconds: 0
    };

    sqs.receiveMessage(params, function(err, data) {
    if (err) {
        console.log("Receive Error", err);
    } else if (data.Messages) {
        console.log("number", data.Messages.length);
        console.log("body", data.Messages[0].Body);
        var deleteParams = {
        QueueUrl: queueURL,
        ReceiptHandle: data.Messages[0].ReceiptHandle
        };
        sqs.deleteMessage(deleteParams, function(err, data) {
        if (err) {
            console.log("Delete Error", err);
        } else {
            console.log("Message Deleted", data);
        }
        });
    }
    });
}

exports.sendTweetsToQueue = function (req, res)  {
    let tweet = JSON.parse(req.body.tweet);
    var params = {
        DelaySeconds: 10,
        MessageBody: JSON.stringify(tweet),
        QueueUrl: queueURL
    };
       
    sqs.sendMessage(params, function(err, data) {
        if (err) {
        console.log("Error", err);
        } else {
        console.log("Success", data.MessageId);
        }
    });
}
// sendTweetsToQueue();
// getTweetsFromQueue();