const Analyzer = require('natural').SentimentAnalyzer;
const stemmer = require('natural').PorterStemmer;
const analyzer = new Analyzer("English", stemmer, "afinn");
const sw = require('stopword');
const natural = require('natural');
const fs = require("fs");
const text = fs.readFileSync("./api/stopwords.txt","utf-8");
// const text = fs.readFileSync("./api/twitter-stopwords.txt","utf-8");
const tokenizer = new natural.WordTokenizer();

// function readFiles(dirname, onFileContent, onError) {
//     fs.readdirSync(dirname, function(err, filenames) {
//             if (err) {
//             onError(err);
//             return;
//         }
//         filenames.forEach(function(filename) {
//             fs.readFile(dirname + filename, 'utf-8', function(err, content) {
//                 if (err) {
//                 onError(err);
//                 return;
//                 }
//                 onFileContent(filename, content);
//             });
//         });
//     });
// }

exports.sentimentAnalysis = function(sentence) {
    return analyzer.getSentiment(tokenizer.tokenize(sentence));
}

exports.tokenize = function(sentence) {
    // get tweet stop words
    var tweet_stopwords = text.split("\n");
    // var tweet_stopwords = text.split(",");
    // var tweet_stopwords = [];
    // readFiles('./api/stopwords', function(filename, content) {
    //     tweet_stopwords.push(...content.split(','));
    // }, function(err) {
    //     throw err;
    // });
    // remove numbers and stopwords and lower case
    return sw.removeStopwords(tokenizer.tokenize(sentence.replace(/[0-9]/g, '').toLowerCase()), tweet_stopwords);
}

exports.wordFreq = function(words) {
    var counts = words.reduce(function ( stats, word ) {

        /* `stats` is the object that we'll be building up over time.
           `word` is each individual entry in the `matchedWords` array */
        if ( stats.hasOwnProperty( word ) ) {
            /* `stats` already has an entry for the current `word`.
               As a result, let's increment the count for that `word`. */
            stats[ word ] = stats[ word ] + 1;
        } else {
            /* `stats` does not yet have an entry for the current `word`.
               As a result, let's add a new entry, and set count to 1. */
            stats[ word ] = 1;
        }

        /* Because we are building up `stats` over numerous iterations,
           we need to return it for the next pass to modify it. */
        return stats;

    }, {} );

    let results = [];

    for (var key in counts) {
        var value = counts[key];
        results.push({text: key, value: value});
    }

    results.sort(function(a,b){return b.value - a.value;});
    return results.slice(0,100);
}
