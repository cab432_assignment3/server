const Twitter = require('twitter');
const config = require('./config.js');
const twitter = new Twitter(config);

// Standard search API
// twitter.get(path, params, callback);
exports.getTweets = function(param) {
    return new Promise((resolve, reject) => {
        twitter.get('search/tweets', param, function(error, tweets, response) {
            if (error) {
                reject(error);
            } else {
                // console.log(tweets.statuses);
                resolve(tweets.statuses);
            }
        });
    });
}

// Premium Search API
exports.premiumSearch = function(param) {
    return new Promise((resolve, reject) => {
        twitter.get('tweets/search/:product/tweets', param, function(error, tweets, response) {
            if (error) {
                reject(error);
            } else {
                resolve(tweets.results);
            }
        });
    });
}


/**
 * Streaming API
 * Stream statuses filtered by keyword
 * number of tweets per second depends on topic popularity
 **/
exports.streamSearch = function() {
    const search = "apple"
    return new Promise((resolve, reject) => {
        twitter.stream('statuses/filter', {track: search}, function(stream) {
            let tweets = [];
            let count = 0;
            const MAX_TWEETS = 100;
            stream.on('data', function(tweet) {
                // console.log(tweet.text);
                count = count + 1
                console.log(count);
                tweets.push(tweet);
                // resolve(tweet.text);
                if (tweets.length >= MAX_TWEETS){
                    resolve(tweets);
                }
            });
            stream.on('error', function(error) {
                reject(error);
            });
        });
    });
}
